# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential wget libtool autotools-dev autoconf libssl-dev git-core unzip libevent-dev libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN wget -P /opt https://github.com/CloakProject/2.0.2.1-Wallets/raw/master/cloakCoin_qt-daemon_linux_x64_v2.0.2.1.defender.zip && \
    cd /opt && \
	unzip /opt/cloakCoin_qt-daemon_linux_x64_v2.0.2.1.defender.zip -d /opt && \
	chmod +x /opt/cloakcoind && \
	chmod +x /opt/cloakcoin-qt

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libevent-dev libdb4.8 libdb4.8++ libcurl3 libcurl3-gnutls libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r cloakcoin && useradd -r -m -g cloakcoin cloakcoin
RUN mkdir /data
RUN chown cloakcoin:cloakcoin /data
COPY --from=build /opt/cloakcoind /usr/local/bin/cloakcoind
USER cloakcoin
VOLUME /data
EXPOSE 12788 12789
CMD ["/usr/local/bin/cloakcoind", "-datadir=/data", "-conf=/data/CloakCoin.conf", "-server", "-txindex", "-printtoconsole"]